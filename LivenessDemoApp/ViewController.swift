//
//  ViewController.swift
//  LivenessDemoApp
//
//  Created by Thadeu Barros on 09/03/22.
//

import UIKit
import BigIDLivenessIOSFramework


class ViewController: UIViewController {

    @IBOutlet var LivenessImg: UIImageView!
    @IBOutlet var LivenessMsg: UILabel!
    @IBAction func didTapButton(_ sender: Any) {
       
        if(Liveness.IsDeviceSupported()){
            let livenessView = Liveness.createViewController()
            let config = Configuration()
            config.faceOutline = true
            
            livenessView.TOKEN = "SEU TOKEN AQUI"
            
            livenessView.Config = config
            //livenessView.Config!.imageDrawable = UIImage(named:"stofLogo")!
            //livenessView.Config!.backgroundColor = .white
            //livenessView.Config!.textColor = .black

            livenessView.LivenessCallback = {
               (LivenessResult) in
                self.LivenessImg.image = LivenessResult.ResultImage
                self.LivenessMsg.text = LivenessResult.HashMd5
            }
            present(livenessView, animated: true, completion: nil)
        }else{
            self.LivenessMsg.text = "Device not supported!"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        LivenessImg.isUserInteractionEnabled = true
        LivenessImg.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let activityViewController = UIActivityViewController(activityItems: [LivenessImg.image!], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                
                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
    }
}


